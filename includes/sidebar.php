
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="<?=BASE_URL . 'index.php' ?>" class="brand-link">
    <img src="<?= BASE_URL . 'assets/img/logo.png' ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Global Vigilance </span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-flat" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <?php
        if (is_admin()) { ?>         
          <li class="nav-item has-treeview">
            <a href="<?= BASE_URL . 'admin/stores.php' ?>" class="nav-link">
              <i class="nav-icon fas fa-th-large"></i>
              <p>
                Stores
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?= BASE_URL . 'admin/users.php' ?>" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?= BASE_URL . 'admin/records.php' ?>" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Records
              </p>
            </a>            
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-pen"></i>
              <p>
                Remarks
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= BASE_URL . 'admin/remarks.php' ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Remarks</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= BASE_URL . 'admin/remarks_request.php' ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Custom Remarks 
                  <?php
                    $remarks = $db->get_single('select count(id) as cnt from remarks where is_custom="1" and is_active="0" ');
                    if($remarks['cnt'] > 0){  ?>
                  <span class="badge badge-info right"><?=$remarks['cnt']?></span>
                    <?php }
                  ?>
                  </p>                  
                </a>
              </li>             
            </ul>
          </li>
        <?php
        } else {
        ?>        
          <li class="nav-item has-treeview">
            <a href="<?= BASE_URL . 'user/records.php' ?>" class="nav-link">
              <i class="nav-icon fas fa-pen"></i>
              <p>
                Records
              </p>
            </a>
            
          </li>

        <?php } ?>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
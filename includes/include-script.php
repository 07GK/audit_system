<!-- jQuery -->
<script src="<?=BASE_URL.'assets/js/jquery/jquery.min.js'?>"></script>
<!-- jQuery Cookie-->
<script src="<?=BASE_URL.'assets/js/jquery/jquery.cookie.min.js'?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=BASE_URL.'assets/js/bootstrap/bootstrap.bundle.min.js'?>"></script>
<!-- AdminLTE App -->
<script src="<?=BASE_URL.'assets/js/dist/adminlte.min.js'?>"></script>
<!-- Date & Range -->
<script type="text/javascript" src="<?= BASE_URL.'assets/js/daterangepicker/moment.min.js'?>"></script>
<script type="text/javascript" src="<?= BASE_URL.'assets/js/daterangepicker/daterangepicker.min.js'?>"></script>
<!-- Select2 -->
<script src="<?=BASE_URL.'assets/js/select2/select2.full.min.js'?>"></script>
<!-- Datatables -->
<script src="<?=BASE_URL.'assets/js/datatables/jquery.dataTables.min.js'?>"></script>
<!-- DropZone js -->
<script src="<?=BASE_URL.'assets/js/dropzone/dropzone.js'?>"></script>
<!-- Validate js -->
<script src="<?=BASE_URL.'assets/js/jquervalidator/jquery.validate.min.js'?>"></script>
<!-- EkkoLightBox -->
<script src="<?=BASE_URL.'assets/js/ekko-lightbox/ekko-lightbox.js'?>"></script>
<!-- Custom Js -->
<script src="<?=BASE_URL.'assets/js/custom.js'?>"></script>
<?php
if(isset($_SESSION['flash']['error'])){               
    toastr('success',$_SESSION['flash']['message']); 
    unset_flash_session($_SESSION['flash']);   
}

?>
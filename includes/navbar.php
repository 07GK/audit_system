<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-light ">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <?php     
    if(!is_admin()){
        ?>
          <div class="form-group col-md-4 m-0">
        <select class='form-control col-md-6 store_selection' data-method="get_store_wise_remarks" data-placeholder="Select Store">
            <?php
            
            $userdata = json_decode($_COOKIE['user_data'], true);
            $user_id = (isset($userdata['id']))?$userdata['id']:$_SESSION['user_data']['id'];
            $storesData = $db->get_all("select s.* from stores s inner join stores_users usj on s.id=usj.store_id where s.is_active = 1 and usj.user_id = ? group by usj.store_id",[$user_id]);
            if(!empty($storesData)){
                if(!isset($_COOKIE['store_id'])){
                    $_SESSION['store_id'] = $storesData[0]['id'];
                    setcookie('store_id', $storesData[0]['id'] , time() + (86400 * 30),'/');  
                }
            }
            $i=0;         
            if (!empty($storesData)) {                                
                echo '<option value="">Select Stores</option>';
                foreach ($storesData as $row) {
                    if(( isset($_COOKIE['store_id']) && $_COOKIE['store_id'] == $row['id'] ) || ( isset($_SESSION['store_id']) && $_SESSION['store_id'] == $row['id'] ) ){
                        echo '<option value=' . $row['id'] . ' selected>' . ucfirst($row['name']) . '</option>';                    
                    }else{                       
                        $sel = ($i==0) ? 'selected' : '';
                        echo '<option value="' . $row['id'] . '" '.$sel.' >' . ucfirst($row['name']) . '</option>';
                    }
                    $i++;
                }                                                
            } else {
                echo '<option>No stores are alloted to you</option>';
            }
            ?>
        </select>
    </div>
        <?php
    }
    ?>              
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                <i class="fa fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <?php
                if( is_user_logged_in()  && !is_admin()){
                    $userdata = json_decode($_COOKIE['user_data'], true);
                    $userdata = (isset($userdata['id']))?$userdata:$_SESSION['user_data'];

                    ?>
                    <a href="#" class="dropdown-item">Welcome <b><?=ucfirst($userdata['first_name']).' '.ucfirst($userdata['last_name'])?></b> ! </a>
                <?php }else{ ?>
                    <a href="#" class="dropdown-item">Welcome <b>Admin </b> ! </a>
                <?php }
                ?>                
                <a href="<?= BASE_URL . 'auth/logout.php' ?>" class="dropdown-item">
                    <i class="fa fa-sign-out-alt mr-2"></i> Log Out
                </a>
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
<div id="loading">
    <div class="lds-ring">
        <div></div>
    </div>
</div>
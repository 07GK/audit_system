<?php
include '../class/class.php';
if (!is_user_logged_in()) {
  header('location:'.BASE_URL . 'index.php');
}

if(is_user_logged_in() && is_admin()){
  header('location:'.BASE_URL . 'admin/stores.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Manage Records</title>
  <?php
  include '../includes/include-css.php';
  ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed ">
  <div class=" wrapper ">
    <?php include '../includes/sidebar.php';
    include '../includes/navbar.php';
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">View Records Details</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Records</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class='row'>
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-body">
                  <div class="col-md-12 mb-3 text-right">
                    <a href="<?= BASE_URL . 'user/add_record.php' ?>" class="d-inline btn btn-block btn-outline-primary col-md-2">Add Record</a>
                  </div>
                  <div class="row col-md-12">
                    <div class="form-group col-md-4">
                      <label>Date range:</label>
                      <div class="input-group col-md-12">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="far fa-clock"></i></span>
                        </div>
                        <input type="text" class="form-control float-right" id="datepicker">
                        <input type="hidden" id="start_date" class="form-control float-right">
                        <input type="hidden" id="end_date" class="form-control float-right">
                      </div>
                      <!-- /.input group -->
                    </div>
                    <div class="form-group col-md-4 d-flex align-items-center pt-4">
                      <!-- <button type="button" class="btn btn-outline-primary btn-sm" onclick="date_wise_search()">Filter</button> -->
                      <button type="button" class="btn btn-outline-danger btn-sm ml-3" id="download_pdf" >Download PDF</button>
                    </div>                    
                  </div>
                  <!-- Table -->
                  <table id='storeTable' class='display dataTable' data-url='apis/get_records_details.php'>

                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th data-orderable='false'>Remarks</th>
                        <th data-orderable='false'>Image</th>
                        <th data-orderable='false'>Actions</th>
                      </tr>
                    </thead>

                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->
  </div>
  <?php include '../includes/footer.php'; ?>
</body>
<?php include '../includes/include-script.php'; ?>

</html>
    <?php
    include 'class/class.php';
    if (is_user_logged_in()) {        
        if(!is_admin()){
          header('location:'.BASE_URL . 'user/records.php');
        }else{
            header('location:'.BASE_URL . 'admin/stores.php');  
        }
    }
  
    ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Login Panel</title>
      <?php
      include 'includes/include-css.php';
      ?>
    </head>

    <body class="hold-transition login-page">
      <div class="login-box">
        <div class="card">
          <!-- login-logo -->
          <div class="login-logo">
            <div>
              <img src='<?= BASE_URL . 'assets/img/logo.png' ?>'>
            </div>
            <a href="../../index2.html"><b>Global Vigilance</b></a>
          </div>
          <!-- /.login-logo -->
          <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <form class="loginForm" action='<?= BASE_URL . 'auth/check_user.php' ?>' method="post">
              <div class="card-body">
                <div class="form-group mt-3">
                  <input type="email" class="form-control" name='email' placeholder="Email">
                </div>
                <div class="form-group mt-3">
                  <input type="password" class="form-control" name='password' placeholder="Password">
                </div>
              </div>

              <div class="row">
                <div class="col-8">
                  <div class="icheck-primary">
                    <input type="checkbox" name='remember_me' id="remember">
                    <label for="remember">
                      Remember Me
                    </label>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-4">
                  <button type="submit" class="btn btn-primary btn-block submitBtn">Sign In</button>
                </div>
                <!-- /.col -->
              </div>
              <div id='errorMessage' class='rounded p-1 m-1 text-center d-none'></div>
            </form>
          </div>
          <!-- /.login-card-body -->
        </div>
      </div>
      <!-- /.login-box -->
    </body>
    </body>
    <?php
    include 'includes/include-script.php';
    ?>

    </html>
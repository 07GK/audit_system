<?php
include '../class/class.php';
if (!is_user_logged_in() || !is_admin()) {
    header('location:' . BASE_URL . 'index.php');
}

$storesData = $db->get_all("select * from stores where is_active = 1 ");

if (isset($_GET['edit_id'])) {
    $_GET['edit_id'] = $db->decrypt($_GET['edit_id']);
    if($db->is_exist('audit_records',['id'=>$_GET['edit_id']])){  
        $records_details = $db->get_single("select ar.*,r.is_custom,r.remarks from audit_records ar left join remarks r on ar.remark_id = r.id where ar.id = ? and ar.is_active=1",[$_GET['edit_id']]);            
        $remarks_data = $db->get_all("select r.id,r.remarks from remarks r inner join stores s on s.id = r.store_id where r.store_id = ? and r.is_custom='0' and r.is_active='1' ",[$records_details['store_id']]);                                                            

    }else{
        header('location:records.php');
    }        
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= (isset($records_details)) ? 'Update Records' : 'Add Records'; ?></title>
    <?php
    include '../includes/include-css.php';
    ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div class=" wrapper ">
        <?php include '../includes/sidebar.php';
        include '../includes/navbar.php';
        ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Manage Records</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Records</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class='row'>
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><?= (isset($records_details)) ? 'Update Records' : 'Add Records'; ?></h3>
                                </div>
                                <!-- /.card-header -->
                                <form id="add_record" action='<?= BASE_URL . 'apis/set_record.php' ?>' method="post">
                                    <?php if (isset($records_details)) { ?>
                                        <input type='hidden' name="editId" value='<?= $records_details['id'] ?>'>
                                        <input type='hidden' name="remarkId" value='<?= $records_details['remark_id'] ?>'>
                                    <?php } ?>                                    
                                    <div class="card-body">
                                        <div id='errorMessage' class='rounded p-1 m-1 text-center d-none col-6 m-auto'></div>
                                        <div class='row'>
                                            <div class="form-group col-md-4">
                                                <label for="store_id"> Select Store *</label>
                                                <select class="form-control multipleSelect select_stores" data-allow-clear='true' name='store_id' data-method="get_store_wise_remarks" data-placeholder='Search and select stores'>
                                                    <?php
                                                    if (!empty($storesData)) {
                                                        echo '<option value=""></option>';
                                                        foreach ($storesData as $row) {
                                                            
                                                            $selected = ( $row['id'] == $records_details['store_id'] )  ? 'selected' : '';
                                                            echo '<option value="' . $row['id'] . '" '.$selected.' >' . $row['name'] . '</option>';
                                                        }
                                                    } else {
                                                        echo '<option>No stores are available</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="date">Select Date *</label>
                                                <input type="date" name="date" class="form-control" value="<?= (isset($records_details)) ? $records_details['date'] : '' ?>">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="time">Select Time *</label>
                                                <input type="time" name="time" class="form-control" value="<?= (isset($records_details)) ? $records_details['time'] : '' ?>">
                                            </div>
                                        </div>                                        
                                        <div class="row">                                            
                                            <div class="form-group col-md-6">
                                                <label for="remark">Remark *</label>
                                                <select class="form-control remark" data-allow-clear='true' id='selected_remarks' name='remark' data-placeholder='Search and select remark'>
                                                    <?php
                                                    $flag = 0;
                                                    if (!empty($remarks_data)) {                                                        
                                                        foreach ($remarks_data as $row) {
                                                            $selected = ( $row['id'] == $records_details['remark_id'] )  ? 'selected' : '';
                                                            echo '<option value="' . $row['id'] . '" '.$selected.' >' . $row['remarks'] . '</option>';
                                                        }
                                                    } else {
                                                        echo '<option></option>';
                                                    }       
                                                    if(isset($records_details) && $records_details['is_custom']==1){                                                                      
                                                        echo '<option value="custom_remark" selected >Custom remark</option>';    
                                                        $flag=1;                                                                                                                            
                                                    }                                             
                                                    ?>
                                                </select>
                                            </div>
                                            <?php 
                                            if($flag==1){ ?>
                                                <div class="form-group col-md-6 custom_remark">
                                                    <label for="time">Custom Remark *</label>
                                                    <input type="text" class='form-control' name="custom_remark" value='<?= $records_details['remarks'] ?>' placeholder="Enter Custom Remark Here">                                                    
                                                    <input type="hidden" class='form-control' name="custom_remark_id" value='<?= $records_details['remark_id'] ?>'>                                                    
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col">
                                                <label for="image">Upload Image <?= (isset($records_details))  ? '<span class="text-danger">Upload only if required  </span>' : '*' ?></label>
                                                <label class="text-danger img_required d-none">Image is required</label>                                                
                                                <div id="upload_file" name="image" class="dropzone"></div>
                                            </div>   
                                            <?php if(isset($records_details['id'])){ ?>
                                                <div class="form-group col">                                                    
                                                    <label>Preview Image </label>                                                    
                                                    <div class='preview_img_container'>
                                                        <img src="<?=BASE_URL. $records_details['image_path']?>">
                                                    </div>
                                                </div>  
                                            <?php } ?>                                           
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">                                        
                                        <?php if(!isset($records_details)){ ?>
                                        <button type="submit"  class="btn btn-success mr-3 submitBtn" data-action='close'>Save & Close</button>
                                        <button type="submit"  class="btn btn-warning submitBtn" data-action='next'>Save & Next</button>
                                        <?php }else{ ?>
                                            <button type="submit"  class="btn btn-success mr-3 submitBtn" data-action='close'>Update record</button>
                                        <?php } ?>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php include '../includes/footer.php'; ?>
</body>
<?php include '../includes/include-script.php'; ?>

</html>
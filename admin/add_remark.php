<?php
include '../class/class.php';
if (!is_user_logged_in() || !is_admin()) {
    header('location:' . BASE_URL . 'index.php');
}
if (isset($_GET['edit_id'])) {
    if ($db->is_exist('remarks', ['id' => $db->decrypt($_GET['edit_id'])])) {
        $remark_details = $db->get_single("select * from remarks where id = ?", [$db->decrypt($_GET['edit_id'])]);
    } else {
        header('location:remarks.php');
    }
}

$storesData = $db->get_all("select * from stores where is_active = 1");

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= (isset($remark_details)) ? 'Update Remarks' : 'Add Remarks'; ?></title>
    <?php
    include '../includes/include-css.php';
    ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div class=" wrapper ">
        <?php include '../includes/sidebar.php';
        include '../includes/navbar.php';
        ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Manage Remarks</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Remarks</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class='row'>
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><?= (isset($remark_details)) ? 'Update Remark' : 'Add Remark'; ?></h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form class="formSubmitEvent" id='add_remark' action='<?= BASE_URL . 'apis/set_remark.php' ?>' method="post">
                                    <?php if (isset($remark_details)) { ?>
                                        <input type='hidden' name="edit_id" value='<?= $_GET['edit_id'] ?>'>
                                    <?php } ?>
                                    <div class="card-body row">
                                        <div id='errorMessage' class='rounded p-1 m-1 text-center d-none col-6 m-auto'></div>
                                        <?php
                                            if(!isset($_GET['edit_id'])){ ?>
                                            <div class="form-group col">                                            
                                                <label for="address">Select Stores *</label>
                                                <select class="form-control multipleSelect" name='store_id' data-placeholder='Search and select stores'>
                                                    <?php
                                                    if (!empty($storesData)) {                                                    
                                                        foreach ($storesData as $row) {
                                                            if ($row['id'] == $remark_details['store_id']) {
                                                                echo '<option value=' . $row['id'] . ' selected >' . $row['name'] . '</option>';
                                                            } else {
                                                                echo '<option value=' . $row['id'] . '>' . $row['name'] . '</option>';
                                                            }
                                                        }
                                                    } else {
                                                        echo '<option>No stores are available</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                        <?php }else {                                                                                          
                                            ?>
                                            <div class="form-group col">
                                                <label for="remark">Store Name </label>
                                                <input type="text" class="form-control" readonly value='<?=$storesData[array_search($remark_details['store_id'],array_column($storesData,'id'))]['name']?>'>
                                                <input type="hidden" name="store_id" value="<?=$remark_details['store_id']?>">
                                            </div>
                                        <?php }
                                        ?>
                                        <div class="form-group col">
                                            <label for="remark">Remark *</label>
                                            <input type="text" class="form-control" name='remark' placeholder="Enter Remark Here" value='<?= (isset($remark_details)) ? ucfirst($remark_details['remarks']) : ''; ?>'>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary submitBtn"><?= (isset($remark_details)) ? 'Update Remark' : 'Add Remark'; ?></button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php include '../includes/footer.php'; ?>
</body>
<?php include '../includes/include-script.php'; ?>

</html>
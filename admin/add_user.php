<?php
include '../class/class.php';
if (!is_user_logged_in() || !is_admin()) {    
    header('location:'.BASE_URL . 'index.php');
}
$storesData = $db->get_all("select * from stores where is_active = 1");
if (isset($_GET['edit_id'])) {
    
    if($db->is_exist('users',['id'=>$db->decrypt($_GET['edit_id'])])){  
        $currentUsersInStore = $db->get_single("select u.*, group_concat(distinct s.id) as store_ids from users u inner join stores_users usj on usj.user_id=u.id inner join stores s on s.id=usj.store_id  where u.id = ? and u.is_active=1 ",[$db->decrypt($_GET['edit_id'])]);                    
    }else{
        header('location:users.php');
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= (isset($currentUsersInStore)) ? 'Update Users' : 'Add Users'; ?></title>
    <?php
    include '../includes/include-css.php';
    ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div class=" wrapper ">
        <?php include '../includes/sidebar.php';
        include '../includes/navbar.php';
        ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Manage Users</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Users</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class='row'>
                        <div class="col-md-12">                            
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><?= (isset($currentUsersInStore)) ? 'Update User' : 'Add User'; ?></h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form class="formSubmitEvent" id="add_user" action='<?= BASE_URL . 'apis/set_user.php' ?>' method="post">
                                    <?php if (isset($currentUsersInStore)) { ?>
                                        <input type='hidden' name="edit_id" value='<?= $_GET['edit_id'] ?>'>
                                    <?php } ?>
                                    <div class="card-body">
                                        <div id='errorMessage' class='rounded p-1 m-1 text-center d-none col-6 m-auto'></div>
                                        <div class="form-group">
                                            <label for="first_name">First Name *</label>
                                            <input type="text" class="form-control" name='first_name' placeholder="Enter First Name Here" value='<?= (isset($currentUsersInStore)) ? $currentUsersInStore['first_name'] : ''; ?>'>
                                        </div>
                                        <div class="form-group">
                                            <label for="last_name">Last Name *</label>
                                            <input type="text" class="form-control" name='last_name' placeholder="Enter Last Name Here" value='<?= (isset($currentUsersInStore)) ? $currentUsersInStore['last_name']  : ''; ?>'>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email *</label>
                                            <input type="text" class="form-control" name="email" autocomplete="new-email" placeholder="Enter Email Here" value='<?= (isset($currentUsersInStore)) ? $currentUsersInStore['email']  : ''; ?>'>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password<?= (isset($currentUsersInStore)) ? " <span class='text-danger'>Leave blank if no change</span>" : ' *' ?></label>
                                            <input type="password" class="form-control" autocomplete="new-password" name="password" placeholder="Enter Password Here">
                                        </div>
                                        <div class="form-group">
                                            <label for="address">Select Stores</label>
                                            <select class="form-control multipleSelect" name='selected_stores[]' multiple="multiple" data-placeholder='Search and select stores'>
                                                <?php
                                                if (!empty($storesData)) {
                                                    foreach ($storesData as $row) {
                                                        if (in_array($row['id'], explode(',', $currentUsersInStore['store_ids']))) {
                                                            echo '<option value=' . $row['id'] . ' selected >' . $row['name']  . '</option>';
                                                        } else {
                                                            echo '<option value=' . $row['id'] . '>' . $row['name']  . '</option>';
                                                        }
                                                    }
                                                } else {
                                                    echo '<option>No stores are available</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary submitBtn"><?= (isset($currentUsersInStore)) ? 'Update User' : 'Add User'; ?></button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php include '../includes/footer.php'; ?>
</body>
<?php include '../includes/include-script.php'; ?>
</html>
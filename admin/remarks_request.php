<?php 
    include '../class/class.php'; 
    if(!is_user_logged_in() || !is_admin()){
        header('location:'.BASE_URL . 'index.php');
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Custom Remarks Request</title>
    <?php
    include '../includes/include-css.php';
    ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed ">
    <div class=" wrapper ">
    <?php include '../includes/sidebar.php';
      include '../includes/navbar.php';  
?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
                  <!-- Content Header (Page header) -->
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Custom Remarks Details</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Remarks</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class='row'>
            <div class="col-md-12">                        
                <!-- general form elements -->
                <div class="card card-primary">                                        
                    <div class="card-body">                           
                        <!-- Table -->
                        <table id='storeTable' class='display dataTable' data-url='apis/get_remark_details.php?flag=get_request'>

                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Store</th>
                            <th>Remark</th>                            
                            <th>Action</th>                            
                        </tr>
                        </thead>

                        </table>                                                 
                    </div>                                
                </div>
                <!-- /.card -->
            </div>
        </div>
      </div>
    </section>           
        </div>
        <!-- /.content-wrapper -->
    </div>
    <?php include '../includes/footer.php'; ?>
</body>
<?php include '../includes/include-script.php'; ?>

</html>
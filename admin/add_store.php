<?php
include '../class/class.php';

if (!is_user_logged_in() || !is_admin()) {    
    header('location:'.BASE_URL . 'index.php');
}
$usersData = $db->get_all("select u.* from users u inner join roles r on u.id != r.user_id and u.is_active=1");

if(isset($_GET['edit_id'])){        
    if($db->is_exist('stores',['id'=>$db->decrypt($_GET['edit_id'])])){    
        $currentUsersInStore = $db->get_single("select s.*, CONCAT(u.first_name,' ',u.last_name) as user_name ,GROUP_CONCAT(u.id) as user_ids from  stores s join stores_users usj on usj.store_id=s.id join users u on u.id=usj.user_id inner join roles r on u.id != r.user_id where s.id = ? ",[$db->decrypt($_GET['edit_id'])]);                                          
    }else{
        header('location:stores.php');
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= (isset($currentUsersInStore)) ? 'Update Stores' : 'Add Stores'; ?></title>
    <?php
    include '../includes/include-css.php';
    ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div class=" wrapper ">
        <?php include '../includes/sidebar.php';
        include '../includes/navbar.php';
        ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Manage Stores</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Stores</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class='row'>
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><?= (isset($currentUsersInStore)) ? 'Update Store' : 'Add Store'; ?></h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form class="formSubmitEvent" id='add_store' action='<?= BASE_URL . 'apis/set_store.php' ?>' method="post">
                                    <input type='hidden' name='method' value="add" >
                                    <?php if (isset($currentUsersInStore)) { ?>
                                        <input type='hidden' name="edit_id" value='<?= $_GET['edit_id'] ?>'>
                                    <?php } ?>
                                    <div class="card-body">
                                        <div id='errorMessage' class='rounded p-1 m-1 text-center d-none col-6 m-auto'></div>
                                        <div class="form-group">
                                            <label for="name">Store Name *</label>
                                            <input type="text" class="form-control" name='name' placeholder="Enter Name Here" value='<?= (isset($currentUsersInStore)) ? $currentUsersInStore['name']  : ''; ?>'>
                                        </div>
                                        <div class="form-group">
                                            <label for="address">Address *</label>
                                            <input type="text" class="form-control" name="address" placeholder="Enter Address Here" value='<?= (isset($currentUsersInStore)) ?  $currentUsersInStore['address'] : ''; ?>'>
                                        </div>
                                        <div class="form-group">
                                            <label for="address">Select Users</label>
                                            <select class="form-control multipleSelect" name='selectedUsers[]' multiple="multiple" data-placeholder='Search and select users'>
                                                <?php
                                                if (!empty($usersData)) {
                                                    foreach ($usersData as $row) {
                                                        if (in_array($row['id'], explode(',', $currentUsersInStore['user_ids']))) {
                                                            echo '<option value="' . $row['id'] . '" selected >' .$row['first_name'].' '.$row['last_name']. '</option>';
                                                        } else {
                                                            echo '<option value="' . $row['id'] . '">' . $row['first_name'].' '.$row['last_name'] . '</option>';
                                                        }
                                                    }
                                                } else {
                                                    echo '<option>No users are available</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary submitBtn"><?= (isset($currentUsersInStore)) ? 'Update Store' : 'Add Store'; ?></button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>
    <?php include '../includes/footer.php'; ?>
</body>
<?php include '../includes/include-script.php'; ?>

</html>
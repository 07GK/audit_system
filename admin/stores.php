<?php 
    include '../class/class.php'; 
    if(!is_user_logged_in() || !is_admin()){
         header('location:'.BASE_URL . 'index.php');
    }
  if(isset($_GET['user_id']) && !empty($_GET['user_id'])){
    
    $user_data =$db->get_single('select * from users where id = ? and is_active=1',[$db->decrypt($_GET['user_id'])]);            
    $string = "<h4>User : <span class='text-danger'>".ucfirst($user_data['first_name'])." ".ucfirst($user_data['last_name'])."</span></h4>";
    $url = "apis/get_store_details.php?user_id=".$_GET['user_id'];
  }else{
    $string = "";
    $url = "apis/get_store_details.php";
  }    
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Stores</title>
    <?php
    include '../includes/include-css.php';
    ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed ">
    <div class=" wrapper ">
    <?php include '../includes/sidebar.php';
      include '../includes/navbar.php';  
?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
                  <!-- Content Header (Page header) -->
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">View Stores Details</h1>
            <?=$string?>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Stores</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class='row'>
            <div class="col-md-12">                        
                <!-- general form elements -->
                <div class="card card-primary">                                        
                    <div class="card-body">    
                        <div class="col-md-12 mb-3 text-right">                   
                            <a href="<?=BASE_URL . 'admin/add_store.php'?>" class="d-inline btn btn-block btn-outline-primary col-md-2">Add Store</a>
                        </div>
                        <!-- Table -->
                        <table id='storeTable' class='display dataTable' data-url='<?=$url?>'>

                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th <?=(isset($_GET['user_id'])) ? 'data-visible="false"' : ''?>>Total Users</th>
                            <th data-orderable='false'>Actions</th>
                            
                        </tr>
                        </thead>

                        </table>                                                 
                    </div>                                
                </div>
                <!-- /.card -->
            </div>
        </div>
      </div>
    </section>           
        </div>
        <!-- /.content-wrapper -->
    </div>
    <?php include '../includes/footer.php'; ?>
</body>
<?php include '../includes/include-script.php'; ?>

</html>
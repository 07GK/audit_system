<?php
include '../class/class.php';
if(!is_user_logged_in()){
    move( BASE_URL . 'index.php');
}

if(isset($_POST)){                       
    $flag = 1;
    if( isset($_POST['editId']) && empty($_FILES['file']['name'][0]) ){ 
        $flag = 0;    
    }

    if( (isset($_FILES) && !empty($_FILES['file']['name'][0])) || $flag==0 ){                         
        if( !isset($_POST['editId']) || !empty($_FILES['file']['tmp_name'][0])){
            $result = validate_file($_FILES['file'],FILE_URL . UPLOAD_URL);                        
        }
        
        if($result['error']){            
            $response['error'] = true;
            $response['message'] = $result['message'];      
        }else{               
            $userdata = json_decode($_COOKIE['user_data'], true);             
            $user_id = (isset($userdata['id']))?$userdata['id']:$_SESSION['user_data']['id'];         
            $store_id = ( is_admin() ) ? $_POST['store_id'] : get_store_id();
            
            $tmp = [
                'date'=>$_POST['date'],  
                'time'=>$_POST['time'],                  
                'remark_id'=>$_POST['remark'],                  
                'user_id'=>$user_id,                  
                'store_id'=>$store_id,                  
            ];   
            
            if(is_admin() && isset($_POST['editId'])){
                unset($tmp['user_id']);
            }
            if( !isset($_POST['editId']) || !empty($_FILES['file']['tmp_name'][0])){
                $tmp['image_path'] = UPLOAD_URL . $result['data']['name'];  
            }            
            
            $flag = 0;
            if(count(array_filter(array_map('trim', array_values($tmp)))) < count(array_values($tmp)) ){
                $flag=1;    
            }   
            if($flag==0) {
                if(!empty($_FILES['file']['tmp_name'][0])){
                    move_uploaded_file($_FILES['file']['tmp_name'][0],$result['data']['path']);                        
                }   
                if(isset($_POST['custom_remark_id']) && trim($_POST['remark']) != 'custom_remark'){
                    $db->delete('remarks',['id'=>$_POST['custom_remark_id']]);                    
                }

                if(trim($_POST['remark']) == 'custom_remark' ){                                        
                    if(isset($_POST['editId']) && isset($_POST['custom_remark_id']) ){        
                        $db->update('remarks',['store_id'=>$store_id,'user_id'=>$user_id,'remarks'=> trim(filter_var($_POST['custom_remark'], FILTER_SANITIZE_STRING)),'is_custom'=> 1,'is_active'=>0],[ 'id'=>$_POST['custom_remark_id']]);                                        
                        $tmp['remark_id'] = $_POST['custom_remark_id'];
                    }else{
                        $tmp['remark_id'] = $db->insert('remarks',['store_id'=>$store_id,'user_id'=>$user_id,'remarks'=> trim(filter_var($_POST['custom_remark'], FILTER_SANITIZE_STRING)),'is_custom'=> 1,'is_active'=>0]);                                        
                    }
                }

                if(isset($_POST['editId'])){                       
                    $db->update('audit_records',$tmp,['id'=>$_POST['editId']]);
                    $id = $_POST['editId'];
                }else{                                       
                    $id = $db->insert('audit_records',$tmp);                    
                    $db->insert('user_logs',['user_id'=>$tmp['user_id'],'store_id'=>$tmp['store_id'],'audit_record_id'=>$id]);                
                }                    
                $response['error'] = false;
                $response['message'] = (isset($_POST['editId'])) ? 'Details updated successfully' : 'Details added successfully';
                $response['url'] = (is_admin()) ? BASE_URL . 'admin/records.php' : BASE_URL . 'user/records.php';            
                set_flash_session($response['error'],$response['message']);  

            }else{
                $response['error'] = true;
                $response['message'] ='Some fields are required !'; 
            }
            
        }
    }else{
        $response['error'] = true;
        $response['message'] = 'Image is required';    
    }

    header('Content-Type: application/json');
    echo json_encode($response);
}


?>
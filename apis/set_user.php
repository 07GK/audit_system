<?php
include '../class/class.php';
if(!is_user_logged_in()){
    move( BASE_URL . 'index.php');
}

if(isset($_POST)){    
        
    if(isset($_POST['edit_id'])){
        $_POST['edit_id'] = $db->decrypt($_POST['edit_id']);
        if($db->is_exist('users',['email'=>trim($_POST['email']),'is_active'=>1],trim($_POST['edit_id']))){ 
            $response['error'] = true;
            $response['message'] = 'User email already exist!';
            header('Content-Type: application/json');
            echo json_encode($response);
            return false;
        }
    }else{
        if($db->is_exist('users',['email'=>trim($_POST['email']),'is_active'=>1])){ 
            $response['error'] = true;
            $response['message'] = 'User email already exist!';
            header('Content-Type: application/json');
            echo json_encode($response);
            return false;
        }
    }
    $flag=0;        
    $tmp = [
        'first_name'=> filter_var($_POST['first_name'], FILTER_SANITIZE_STRING),
        'last_name'=>filter_var($_POST['last_name'], FILTER_SANITIZE_STRING),
        'email'=>$_POST['email'],
        'password'=>$_POST['password'],
    ];    
    if(isset($_POST['edit_id']) && empty($_POST['password'])){
        unset($tmp['password']);
    }
    
    if(count(array_filter(array_map('trim', array_values($tmp)))) < count(array_values($tmp)) ){
        $flag=1;    
    }    
    
    if($flag==0){              

        if(!filter_var($tmp['email'], FILTER_VALIDATE_EMAIL)) {          
            $response['error'] = true;
            $response['message'] = 'Email is not valid !';
            $flag=1;    
        }

        if( !isset($_POST['edit_id']) && strlen($tmp['password']) < 8){
            $response['error'] = true;
            $response['message'] = 'Password should be at least 8 characters in length ';
            $flag=1;    
        }
        if($flag==1){
            header('Content-Type: application/json');
            echo json_encode($response);
            return false;
        }

        if(!empty($tmp['password'])){
            $tmp['password'] = sha1($tmp['password']);
        }
        if(isset($_POST['edit_id'])){
            $db->update('users',$tmp, ['id'=> $_POST['edit_id']] );
            $id = $_POST['edit_id'];
        }else{                    
            $id = $db->insert('users',$tmp);            
        }
    
        if(isset($_POST['selected_stores'])){
            $user_store_conj = [];            
            foreach ($_POST['selected_stores'] as $val) {                
                array_push($user_store_conj,['user_id'=>$id,'store_id'=>$val]);    
            }                        
            if(isset($_POST['edit_id'])){                                             
                $db->delete('stores_users',['user_id'=>$_POST['edit_id']]);   
            }            
            $db->insert_bulk('stores_users',$user_store_conj);      
        }else{
            if(isset($_POST['edit_id'])){                                             
                $db->delete('stores_users',['user_id'=>$_POST['edit_id']]);   
            }            
        }
        $response['error'] = false;
        $response['message'] = (isset($_POST['edit_id'])) ? 'Details updated successfully' : 'Details added successfully';
        $response['url'] = BASE_URL . 'admin/users.php';            
        set_flash_session($response['error'],$response['message']);  
    }else{
        $response['error'] = true;
        $response['message'] ='Some fields are required !';      
    }  
    header('Content-Type: application/json');
    echo json_encode($response);

}


?>
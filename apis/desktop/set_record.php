<?php
include '../../class/class.php';

if (isset($_POST)) {        
    $flag = 0;
    $tmp = [
        'date' => date('Y-m-d',strtotime($_POST['date'])),
        'time' => $_POST['time'],
        'remark_id' => $_POST['remark_id'],
        'store_id' => $_POST['store_id'],
        'user_id' => USER_ID,        
    ];
    
    if(count(array_filter(array_map('trim', array_values($tmp)))) < count(array_values($tmp)) ){
        $flag=1;    
    }   

    if($flag == 0){

        if (preg_match('/^data:image\/(\w+);base64,/', $_POST['image'], $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif
            $message = '';
            if (!in_array($type, ['jpg', 'jpeg', 'png'])) {
                $message = 'invalid image type';
                $flag=1;    
            }
            $data = str_replace(' ', '+', $data);
            $data = base64_decode($data);
    
            if ($data === false) {
                $message = 'base64_decode failed';
                $flag=1;    
            }
        } else {
            $message = 'did not match data URI with image data';
            $flag=1;    
        }        
        if($flag==0){

            $image_path =  UPLOAD_URL . uniqid(rand(), true).'.'.$type;
            file_put_contents( FILE_URL . $image_path, file_get_contents($_POST['image']));
            
            
            $tmp['image_path'] = $image_path;
    
            $id = $db->insert('audit_records', $tmp);
            $db->insert('user_logs', ['user_id' => $tmp['user_id'], 'store_id' => $tmp['store_id'], 'audit_record_id' => $id]);
        
            $response['status'] = true;
            $response['message'] = 'Details updated successfully';
            $response['data'] = [];

        }else{
            $response['status'] = false;
            $response['message'] =$message; 
            $response['data'] = [];
        }
    }else{
        $response['status'] = false;
        $response['message'] ='Some fields are required !'; 
        $response['data'] = [];
    }
    header('Content-Type: application/json');
    echo json_encode($response);
} 


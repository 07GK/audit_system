<?php
include '../../class/class.php';

$data = $db->get_all("select GROUP_CONCAT(r.remarks) as remarks,GROUP_CONCAT(r.id) as remarks_id,s.name as store_name,s.id,s.address as store_address from remarks r left join stores s on r.store_id = s.id where r.is_custom=0 and r.is_active=1 and s.is_active=1 GROUP by store_id order by store_id desc",[]);                                                            
if(!empty($data)){
    $storeData['store_id'] = [];
    foreach($data as $row){           
        $storeData['store_id'][$row['id']] = [];        
        $storeData['store_id'][$row['id']]['store_name']=$row['store_name'];
        $storeData['store_id'][$row['id']]['store_location']=$row['store_address'];        
        $remarks = explode(',',$row['remarks']);
        $remarks_id = explode(',',$row['remarks_id']);        
        for ($i=0; $i < count($remarks) ; $i++) {             
            $storeData['store_id'][$row['id']]['remarks'][$remarks_id[$i]] = $remarks[$i];
        }        
    }
    $response['status']=true;
    $response['message']='Data retrieved successfully';
    $response['data']=$storeData;
}else{
    $response['status']=false;
    $response['message']='Something went Wrong!';
    $response['data']=[];
}
header('Content-Type: application/json');
echo json_encode($response);

?>


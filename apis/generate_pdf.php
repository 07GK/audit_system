<?php
 
include '../class/class.php';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {      
    class MYPDF extends TCPDF {

        //Page header
        public function Header() {            
            if($this->page > 1){
                $this->SetFont('helvetica', 'BU', 20);
                $this->SetY(10);
                // Title
                $this->Cell(0, 15, 'Auditing Report', 0, false, 'R', 0, '', 0, false, 'M', 'centerB');
            }
        }
    
        // Page footer
        public function Footer() {
            if($this->page > 1){
                // Position at 15 mm from bottom
                $this->SetY(-15);
                // Set font
                $this->SetFont('helvetica', '', 10);                
                $this->writeHTMLCell(0,10,'','','<a href="http://www.globalvigilance.solutions/" style="text-decoration: none;" >www.globalvigilance.solutions</a>',0,0,false,true,'L');
                // $this->Cell(0, 10, '<a href="http://www.globalvigilance.solutions/">http://www.globalvigilance.solutions/</a>', 0, false, 'L', 0, '', 0, false, 'T', 'M');
            }
        }
    }



    ob_start();
    $userdata = json_decode($_COOKIE['user_data'], true);
    $user_id = (isset($userdata['id']))?$userdata['id']:$_SESSION['user_data']['id'];    
    $store_id = get_store_id();    
    
    $betweenDate = '';    
    $filter = [];
    // print_r($_POST);
    if (isset($_POST['start_date']) && isset($_POST['end_date']) && !empty($_POST['start_date']) &&  !empty($_POST['end_date'])) {
        $betweenDate = " and ar.date BETWEEN ? and ? ";       
        array_push($filter,date_format(date_create($_POST['start_date']), "Y-m-d"),date_format(date_create($_POST['end_date']), "Y-m-d"));        
    }
    if(isset($_POST['store_id']) && !empty(trim($_POST['store_id'])) && is_numeric($_POST['store_id']) ){
        $searchQuery.=" and ar.store_id=? ";
        $store_id = $_POST['store_id'];        
        array_push($filter,$store_id);    
    }
  
    if(isset($_POST['user_id']) && !empty(trim($_POST['user_id'])) && is_numeric($_POST['user_id']) ){
        $searchQuery.=" and ar.user_id=? ";
        $user_id = $_POST['user_id'];        
        array_push($filter,$user_id);    
    }  
    
        
    if(is_admin()){                        
        $storeRecords = $db->get_all("select ar.*,s.name as store_name,r.remarks as remarks from audit_records ar inner join stores s on s.id = ar.store_id left join remarks r on r.id=ar.remark_id where ar.is_active='1' " . $betweenDate . $searchQuery." order by ar.date ",$filter);            
    }else{
        array_push($filter,$store_id,$user_id);                
        $storeRecords = $db->get_all("select ar.*,s.name as store_name,r.remarks as remarks from audit_records ar inner join stores s on s.id = ar.store_id left join remarks r on r.id=ar.remark_id where ar.is_active='1' " . $betweenDate . " and ar.store_id = ?  and ar.user_id = ?  order by ar.date ",$filter);    
    }
    // print_r($storeRecords);
    
    if(!empty($storeRecords)){

        $pageLayout = array(211, 300);
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, $pageLayout, true, 'UTF-8', false);

        // enable header and footer
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


        // set font
        $pdf->SetFont('helvetica', '', 10);

        // add a page
        $pdf->AddPage();
        // set bacground image

        // get the current page break margin
        $bMargin = $pdf->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $pdf->getAutoPageBreak();
        // disable auto-page-break
        $pdf->SetAutoPageBreak(false, 0);
        $img_file = BASE_URL.'assets/img/background.jpg';
        $pdf->Image($img_file, 0, 0, $pdf->getPageWidth(),$pdf->getPageHeight() , '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $pdf->setPageMark();                        

        $pdf->SetXY(33,222);        
        $pdf->SetTextColor(34, 91, 181);
        $pdf->SetFont('helvetica', 'B', '20');  
        $pdf->Cell(110, 10, ''. html_entity_decode($storeRecords[0]['store_name'], ENT_QUOTES).'', 0, 1, 'L', 0, '', 1);        
        $pdf->SetFont('helvetica', 'B', '15');  
        $pdf->SetXY(33,230);   
        $pdf->SetTextColor(0, 0, 0);
        if (isset($_POST['start_date']) && isset($_POST['end_date']) && !empty($_POST['start_date']) &&  !empty($_POST['end_date'])) {
            if( strtotime($_POST['start_date']) != strtotime($_POST['end_date']) ){
                $date_title = '('.date_format(date_create($_POST['start_date']), "d M Y").') to ('.date_format(date_create($_POST['end_date']), "d M Y").')';
                $pdf->Cell(110, 10, $date_title, 0, 1, 'L', 0, '', 1);
            }else{
                $date_title = '('.date_format(date_create($_POST['start_date']), "d M Y").')';
                $pdf->Cell(110, 10, $date_title, 0, 1, 'L', 0, '', 1);
            }
        }else{
            $max = max(array_map('strtotime', array_column($storeRecords,'date')));
            $min = min(array_map('strtotime', array_column($storeRecords,'date')));                        
            $date_title = '('.date('d M Y', $min).') to ('.date('d M Y', $max).')';
            $pdf->Cell(110, 10, $date_title, 0, 1, 'L', 0, '', 1);
        }
        $i = 0;
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('helvetica','L', '10');
        $pdf->AddPage();
        $tbl = '';        
        $html="<html><style>img{ max-width: 100%; height: 255px;} </style><body>";        
        foreach ($storeRecords as $row) {                        
            
            // Table with rowspans 
            $tbl .= '                                      
                    <tr>                        
                        <td width="50" height="25" align="center" style="background-color:rgb(140, 173, 250);line-height:30px;" ><b style="color:red;">Date</b> </td>
                        <td width="75" height="25" align="center" style="background-color:rgb(252, 235, 210);line-height:30px;" >'.date('d-m-Y',strtotime($row['date'])).' </td>
                        <td width="50" height="25" align="center" style="background-color:rgb(140, 173, 250);line-height:30px;"><b style="color:red;">Time</b></td>  
                        <td width="60" height="25" align="center" style="background-color:rgb(252, 235, 210);line-height:30px;">'.$row['time'].' </td>  
                        <td width="80" height="25" align="center" colspan="2" style="background-color:rgb(140, 173, 250);line-height:30px;"><b style="color:red;">Remarks</b> </td>  
                        <td width="285" height="25" align="left" colspan="2" style="background-color:rgb(252, 235, 210);line-height:30px;">'.' '.$row['remarks'].' </td>                          
                    </tr>                            
                    <tr>
                        <td width="600" style="line-height:3px;">
                            <table border="0" cellpadding = "1" cellspacing = "1">
                                <tr>                                    
                                    <td width="585" align="center">                            
                                        <img src="'.BASE_URL. str_replace(" ", "%20", trim($row['image_path'])).'">
                                    </td>         
                                </tr>
                            </table>
                        </td>            
                    </tr>                                 
                ';
            $i++;            
            if($i % 3 == 0 || $i==count($storeRecords)){                                
                $html.= '<table border="1" cellpadding = "0" cellspacing = "0" >'.$tbl.'</table>';                
                $tbl = '';
            }              
        }
        $html .= '</body></html>';        
        $pdf->writeHTML($html, true, false, true, false, '');
        $lastPage = $pdf->getPage();
        if(($lastPage - 1) > ceil(count($storeRecords)  / 3) ){
            $pdf->deletePage($lastPage);
        }        
        ob_clean();    
        setcookie('download_flag','1',time() + 180,'/');
        header('Content-Disposition: attachment; filename="report-'.$date_title.'.pdf"');
        header('Content-type: application/pdf');  
        return $pdf->Output('report'.$date_title.'.pdf', 'I');
    }else{
        return false;
    }
}
http_response_code(405);
exit();
?>
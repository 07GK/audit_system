<?php

include '../class/class.php';
if(!is_user_logged_in()){
    move( BASE_URL . 'index.php');
}


## Read value
$draw = $_GET['draw'];
$row = $_GET['start'];
$rowperpage = $_GET['length']; // Rows display per page
$columnIndex = $_GET['order'][0]['column']; // Column index
$columnName = $_GET['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
$searchValue = $_GET['search']['value']; // Search value


## Search 
$searchQuery = " ";
$searchQueryArr = [];
if($searchValue != ''){
   $searchQuery = " and (r.remarks like ? )";   
   $searchQueryArr[] = "%{$searchValue}%";
}


if(isset($_GET['start_date']) && isset($_GET['end_date']) && !empty($_GET['start_date']) &&  !empty($_GET['end_date'])){
  $searchQuery .= " and date BETWEEN ? and ? ";    
  array_push($searchQueryArr,date_format(date_create($_GET['start_date']),"Y-m-d"),date_format(date_create($_GET['end_date']),"Y-m-d"));
}

## Total number of records without filtering

$records = $db->get_single("select count(*) as allcount from audit_records where is_active=1");
$totalRecords = $records['allcount'];


## Total number of record with filtering
if(isset($_GET['type']) && $_GET['type']=='admin'){  
    $columns = array(  
          0=>'id',
          1 =>'store_name',
          2 => 'user_name',
          3 => 'ar.date',
          4 => 'ar.time',          
    );

  if(isset($_GET['selected_store']) && !empty(trim($_GET['selected_store'])) && is_numeric($_GET['selected_store']) ){
      $searchQuery.=" and ar.store_id=? ";
      array_push($searchQueryArr,$_GET['selected_store']);
  }

  if(isset($_GET['selected_user']) && !empty(trim($_GET['selected_user'])) && is_numeric($_GET['selected_user']) ){
      $searchQuery.=" and ar.user_id=? ";
      array_push($searchQueryArr,$_GET['selected_user']);
  }  
  $params = array_merge($searchQueryArr);    
  $records = $db->get_single("select count(*) as allcount from audit_records ar inner join stores s on s.id = ar.store_id inner join users u on u.id = ar.user_id left join remarks r on r.id=ar.remark_id where ar.is_active=1 ".$searchQuery,$params);
  $totalRecordwithFilter = $records['allcount'];

    ## Fetch records
  array_push($params,$row,$rowperpage);
  

  $storeRecords = $db->get_all("select ar.*,s.name as store_name,concat( u.first_name,' ',u.last_name ) as user_name,r.remarks as remarks,r.is_custom,r.is_active from audit_records ar inner join stores s on s.id = ar.store_id inner join users u on u.id = ar.user_id left join remarks r on r.id=ar.remark_id where ar.is_active=1  ".$searchQuery." order by ".$columns[$columnIndex]." $columnSortOrder limit ?,?",$params);
  
}else{
  
  $columns = array(  
    0=>'id',    
    1 => 'ar.date',
    2 => 'ar.time',          
);

  $userdata = json_decode($_COOKIE['user_data'], true);
  $user_id = (isset($userdata['id']))?$userdata['id']:$_SESSION['user_data']['id'];
  $params = array_merge(array(get_store_id() , $user_id),$searchQueryArr);  
  $records = $db->get_single("select count(*) as allcount from audit_records ar left join remarks r on r.id=ar.remark_id where ar.store_id=? and ar.user_id= ? and ar.is_active=1 ".$searchQuery,$params);
  $totalRecordwithFilter = $records['allcount'];

  ## Fetch records
  array_push($params,$row,$rowperpage);
  
  $storeRecords = $db->get_all("select ar.*,r.remarks as remarks,r.is_custom,r.is_active from audit_records ar left join remarks r on r.id=ar.remark_id  where ar.store_id=? and ar.user_id=? and ar.is_active=1  ".$searchQuery." order by ".$columns[$columnIndex]." $columnSortOrder limit ?,?",$params);
  // print_r($storeRecords);
}
$data = [];
$i=$row+1;
foreach ($storeRecords as $row) {

    $url = ( is_admin() ) ? BASE_URL . 'admin/add_records.php?edit_id='. $db->encrypt($row['id']) : BASE_URL . 'user/add_record.php?edit_id='. $db->encrypt($row['id']);    
      $operate = '<a href="'. $url .'" class="btn btn-primary btn-xs mr-1 mb-1" data-id="'.$row['id'].'" title="Edit" ><i class="fa fa-pen" ></i></a>';        
      $operate .= '<a href="javascript:void(0)" class="btn btn-danger btn-xs mr-1 mb-1 delete" data-id="'.$db->encrypt($row['id']).'" data-table="audit_records" title="Delete" ><i class="fa fa-trash" ></i></a>';
      
    $img = '<div class="preview_img"><a href="'.BASE_URL . $row['image_path'].'" data-toggle="lightbox" data-toggle="lightbox"><img src="'.BASE_URL . $row['image_path'].'"  ></a></div>';
    $custum_remark = ($row['is_custom']=='1' && $row['is_active']=='0') ? ' <span class="text-danger"> <br> ( Approval Pending )</span> ' : '';
    if(isset($_GET['type']) && $_GET['type']=='admin'){  
      $data[] = array($i,ucfirst($row['store_name']),ucfirst($row['user_name']),date('d-m-Y',strtotime($row['date'])),$row['time'],ucfirst($row['remarks']).$custum_remark,$img,$operate); 
    }else{
      $data[] = array($i,date('d-m-Y',strtotime($row['date'])),$row['time'],ucfirst($row['remarks']).$custum_remark,$img,$operate);
    }
    
  $i++;
}

## Response
$response = array(
  "draw" => intval($draw),
  "iTotalRecords" => $totalRecords,
  "iTotalDisplayRecords" => $totalRecordwithFilter,
  "aaData" => $data
);

header('Content-Type: application/json');
echo json_encode($response);



?>

<?php
  include '../class/class.php';
    
  if(isset($_POST['email']) && isset($_POST['password'])){
    if($db->is_exist('users',['email'=>$_POST['email'],'password'=>sha1($_POST['password']),'is_active'=>1])){      

      $user_data = $db->get_single("select * from users u where u.email = ? and u.is_active=1 ",[$_POST['email']]);
      unset($user_data['password']);
      $_SESSION['email'] = $_POST['email']; 
      $_SESSION['is_logged_in'] = true;         
      set_flash_session(true,'Logged In Successfully');
      $_SESSION['user_data'] = $user_data;              
    

      if( count($db->get_all("select * from roles r where r.user_id = ?",[$user_data['id']])) > 0){
        $_SESSION['is_admin'] = true;                           
      }
      if(isset($_POST['remember_me'])){        
        setcookie('remember_me', '1', time() + (86400 * 30),'/');
        setcookie('user_data', json_encode($user_data), time() + (86400 * 30),'/');
      }
      $response['error'] = false;
      $response['message'] = 'LoggedIn successfully !';      
      $response['data']['url'] = (is_admin()) ? BASE_URL.'admin/stores.php':BASE_URL.'user/records.php';      
    }else{
      $response['error'] = true;
      $response['message'] = 'Incorrect Login';
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  }  


?>
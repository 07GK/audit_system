<?php

session_start(); 

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include 'connection.php';
include 'db.php';
include 'tcpdf/tcpdf.php';


function refresh() {
    $page = $_SERVER['REQUEST_URI'];
//    $page = $_SERVER['PHP_SELF'];
    echo "<script>window.location='$page';</script>";
}

//this function will be used to redirect to the page 
function move($page) {
    echo "<script>window.location.url='$page';</script>";
}
 
function is_ajax_request() {
    return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
}
 
function is_user_logged_in(){       
    if(isset($_SESSION['is_logged_in']) || isset($_COOKIE['remember_me'])){
        return true;
    }else{
        return false;
    }
}

function is_admin(){
    global $db;
    $userdata = json_decode($_COOKIE['user_data'], true);
    
    $user_email = (isset($userdata['id']))?$userdata['email']:$_SESSION['user_data']['email'];

    $data = $db->get_single("SELECT * FROM `users` `u` JOIN `roles` `r` ON `u`.`id`=`r`.`user_id` WHERE `email` = ? HAVING `r`.`role_type` = 'admin'",[$user_email]);
    
    if(!empty($data)){
        $_SESSION['is_admin'] = true;
        return true;
    }else{
        $_SESSION['is_admin'] = false;
        return false;
    }
}

function validate_file($file,$dir){
    
    $file_path = $dir . trim(basename($file['name'][0]));        
    $extension = pathinfo($file_path, PATHINFO_EXTENSION);    
    $allowedExtensions = ['jpg', 'png', 'gif', 'jpeg'];
    $error = 0;
    $msg = '';
    if (!in_array( strtolower(trim($extension)), $allowedExtensions)) {
      $error = 1;      
      $msg = 'The file have extension which is not allowed.Please only upload files having JPG, PNG and GIF extension';
    }

    if(file_exists($file_path)){                
        //Get the new name if file already exist
        $file_path = rename_path($file_path);                
    }    
    $res['error'] = $error;
    $res['message'] = $msg;
    $res['data'] = [
        'path'=>$file_path,
        'type'=>$extension,
        'name' =>basename($file_path)
    ];
    return $res;
}

function rename_path($path) {        
    if (!file_exists($path)) return $path;
    $fnameNoExt = pathinfo($path,PATHINFO_FILENAME);    
    $ext = ( is_dir($path) ) ? '' : '.'. pathinfo($path, PATHINFO_EXTENSION);
    $i = 1;
    
    $path = ( !empty($ext) ) ?trim(str_replace($ext," ",$path)) : $path;
    
    while(file_exists("$path($i)$ext")){                
        $i++;
    }     
    return ($ext) ? trim($path)."($i)$ext" : "$path($i)" ;

}


function toastr($status,$title){
    echo "<script>Toast.fire({icon: '".$status."',title: '".$title."'});</script>";
}

function set_flash_session($error,$message){
    $_SESSION['flash']['error'] = ($error)?'success':'error';
    $_SESSION['flash']['message'] = $message;
}
function unset_flash_session(){
    unset($_SESSION['flash']['error']);
    unset($_SESSION['flash']['message']);
}

function get_store_id(){
    $store_id = '';
    if(isset($_SESSION['store_id'])){
        $store_id = $_SESSION['store_id'];
    }
    if(isset($_COOKIE['store_id'])){
        $store_id = $_COOKIE['store_id'];
    }
    return $store_id;
}
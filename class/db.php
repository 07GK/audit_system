<?php

class DB {
    public $conn;    
    public function __construct()
	{			
        $this->conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }
    
    function escape_mysql_identifier($field){
        return "`".str_replace("`", "``", $field)."`";
    }
    function add_double_quotes($field){
        return "'".str_replace("'", "``", $field)."'";
    }

    public function insert($table, $data) {
		
        $keys = array_keys($data);
        $keys = array_map(array($this, 'escape_mysql_identifier'), $keys);                				
        $fields = implode(",", $keys);
        $table = $this->escape_mysql_identifier($table);
        $placeholders = str_repeat('?,', count($keys) - 1) . '?';
		$sql = "INSERT INTO $table ($fields) VALUES ($placeholders)";    				
		return $this->prepared_query($this->conn, $sql, array_values($data))->insert_id;		
    }

    public function insert_bulk($table, $data) {                
        $keys = (count($data) > 0 ) ? array_keys($data[0]) : array_keys($data);
		$keys = array_map(array($this, 'escape_mysql_identifier'), $keys);   
		

        $fields = implode(",", $keys);
        $table = $this->escape_mysql_identifier($table);
        $placeholders = str_repeat('?,', count($keys) - 1) . '?';
        $sql = "INSERT INTO $table ($fields) VALUES ";     
        if(count($data) > 0){            
           $sql .= implode(', ', array_fill(0, count($data), "($placeholders)"));         
        }else{
            $sql .= "($placeholders)";
        }        
        $this->prepared_query($this->conn, $sql, array_values($data));
    }
   
    function prepared_query($mysqli, $sql, $params = array(), $types = "")
    {        		
        
        if(is_array($params[0]) && !empty($params)){
            $params = array_map('array_values', $params);                        
            $params = array_merge(...$params);            
		}		
		if(!empty($params)){
			$types = $types ?: str_repeat("s", count($params));		
		}
		try {			
			$stmt = $mysqli->prepare($sql);   				
			if(!empty($params)){
				$stmt->bind_param($types, ...$params);			
			}
			$stmt->execute();						
			return $stmt;
		} catch (Exception $e) {			
			echo $e->getMessage();
		}
    }		

    function is_exist($tbl,$where,$update_id=false){
		
        $sql = "SELECT * from $tbl where ";         
		
		foreach (array_keys($where) as $i => $field) {
            $field = $this->escape_mysql_identifier($field);
            $sql .= ($i) ? " AND " : "";
            $sql .= "$field = ?";
        }

        if($update_id!=false) {
            $sql .= " AND id != ? ";
            $where[] = $update_id;
        }                

        if($this->prepared_query($this->conn, $sql, array_values($where))->get_result()->num_rows > 0){
            return true;     
        }else{
            return false;     
        }        
    }
    
    function select($tbl,$where){		
        $sql = "SELECT * from $tbl where ";                 
		if(!empty($where)){
            foreach (array_keys($where) as $i => $field) {
                $field = $this->escape_mysql_identifier($field);
                $sql .= ($i) ? " AND " : "";
                $sql .= "$field = ?";
            }
        }

        return $this->prepared_query($this->conn, $sql, array_values($where))->get_result()->fetch_all(MYSQLI_ASSOC);
            
	}


    public function delete($tbl, $where,$type='hard') {           
        if($type=='hard'){
            $sql = "DELETE from $tbl where ";         		
        }else{
            $sql = "UPDATE $tbl set `is_active` = '0' where ";         		
        }
		foreach (array_keys($where) as $i => $field) {
            $field = $this->escape_mysql_identifier($field);
            $sql .= ($i) ? " AND " : "";
            $sql .= "$field = ?";
        }
        
        if($this->prepared_query($this->conn, $sql, array_values($where))->affected_rows > 0){
            return true;
        }else{
            return false;     
        }                
    }

    function update($table, $data, $where) {
		
        $table = $this->escape_mysql_identifier($table);
        $sql = "UPDATE $table SET ";
    
        foreach (array_keys($data) as $i => $field) {
            $field = $this->escape_mysql_identifier($field);
            $sql .= ($i) ? ", " : "";
            $sql .= "$field = ?";
        }       
        $sql .= " WHERE ";
        
        foreach (array_keys($where) as $i => $field) {
            $field = $this->escape_mysql_identifier($field);
            $sql .= ($i) ? " AND " : "";
            $sql .= "$field = ?";
		}		
        $data = array_merge($data,array_values($where));                        
        $this->prepared_query($this->conn, $sql, array_values($data));
    }    

	function get_single($query,$data = array()){						     
		return $this->prepared_query($this->conn, $query , $data)->get_result()->fetch_array(MYSQLI_ASSOC);					
	}

	function get_all($query,$data=array()){				
		return $this->prepared_query($this->conn, $query,$data)->get_result()->fetch_all(MYSQLI_ASSOC);					
	}
   
    public function encryptor($action, $string) {
        $output = FALSE;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'dertyu345yu67';
        $secret_iv  = 'dertyu345yu67@GK7';
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning        
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        //do the encryption given text/string/number
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } elseif ($action == 'decrypt') {            
            //decrypt the given text/string/number
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
    
    public function encrypt($data) {        
        return urlencode($this->encryptor('encrypt', filter_var($data,FILTER_SANITIZE_URL)));
    }
    
    public function decrypt($data) {
        return $this->encryptor('decrypt', filter_var($data,FILTER_SANITIZE_URL));
    }

}

$db = new db();              

?>